export interface Environment {
  production: boolean;
  httpApiHost: string;
}
