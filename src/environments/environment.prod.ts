import { Environment } from "./environment.type";

export const environment: Environment = {
  production: true,
  httpApiHost: "http://localhost:4000",
};
