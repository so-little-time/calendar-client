import { HTTP_API_HOST } from "@/core/const/injection-token.const";
import { HttpMethod, HttpOptions } from "@/core/services/http/http.type";
import { HttpClient } from "@angular/common/http";
import { Inject, Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class HttpService {
  constructor(
    @Inject(HTTP_API_HOST) protected apiHost: string,
    protected httpClient: HttpClient,
  ) {}

  public post<T, B = any>(uri: string, options?: HttpOptions): Observable<T> {
    return this.request<T, B>(HttpMethod.POST, uri, options);
  }

  public get<T, B = any>(uri: string, options?: HttpOptions): Observable<T> {
    return this.request<T, B>(HttpMethod.GET, uri, options);
  }

  public put<T, B = any>(uri: string, options?: HttpOptions): Observable<T> {
    return this.request<T, B>(HttpMethod.PUT, uri, options);
  }

  public delete<T, B = any>(uri: string, options?: HttpOptions): Observable<T> {
    return this.request<T, B>(HttpMethod.DELETE, uri, options);
  }

  public request<T, B = any>(
    method: HttpMethod,
    uri: string,
    options?: HttpOptions<B>,
  ): Observable<T> {
    const url = this.resolveUrl(uri);
    // TODO: Add http-error handler abstract class
    return this.httpClient.request<T>(method.toString(), url, options);
  }

  protected resolveUrl(uri: string): string {
    let url = "";
    if (/^(https|http):\/\/.*$/.test(uri)) {
      url = uri;
    } else {
      url = `${this.apiHost}${uri}`;
    }
    return url;
  }
}
