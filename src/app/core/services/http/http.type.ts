export interface HttpHeaders {
  [header: string]: string | string[];
}

export interface HttpParams {
  [param: string]: string | number | boolean | (string | number | boolean)[];
}

export interface HttpOptions<B = any> {
  body?: B;
  headers?: HttpHeaders;
  params?: HttpParams;
}

export enum HttpMethod {
  POST = "POST",
  GET = "GET",
  PUT = "PUT",
  DELETE = "DELETE",
}
