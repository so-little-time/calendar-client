import { InjectionToken } from "@angular/core";

export const HTTP_API_HOST = new InjectionToken("HttpApiHost");
