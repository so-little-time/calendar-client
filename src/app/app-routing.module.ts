import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    redirectTo: "daily",
    pathMatch: "full",
  },
  {
    path: "daily",
    loadChildren: () =>
      import("./pages/daily/daily.module").then((m) => m.DailyModule),
  },
  {
    path: "board",
    loadChildren: () =>
      import("./pages/board/board.module").then((m) => m.BoardModule),
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
