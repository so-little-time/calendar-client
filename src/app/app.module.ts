import { OverlayModule } from "@angular/cdk/overlay";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { environment } from "src/environments/environment";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HTTP_API_HOST } from "./core/const/injection-token.const";
import { SearchBarComponent } from "./shared/components/search-bar/search-bar.component";
import { SharedModule } from "./shared/shared.module";

@NgModule({
  declarations: [AppComponent, SearchBarComponent],
  imports: [
    BrowserModule,
    SharedModule,
    AppRoutingModule,
    HttpClientModule,
    OverlayModule,
  ],
  providers: [{ provide: HTTP_API_HOST, useValue: environment.httpApiHost }],
  bootstrap: [AppComponent],
})
export class AppModule {}
