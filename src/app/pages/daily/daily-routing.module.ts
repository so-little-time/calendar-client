import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { DailyComponent } from "./daily.component";

const routes: Routes = [{ path: "", component: DailyComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DailyRoutingModule {}
