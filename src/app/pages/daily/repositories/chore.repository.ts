import { HttpService } from "@/core/services/http/http.service";
import { ChoreStatus } from "@/daily/const/chore.const";
import { getDateStr } from "@/shared/helpers/date-time.helper";
import { Response } from "@/shared/types/response.type";
import { Injectable } from "@angular/core";
import { classToPlain, plainToClass } from "class-transformer";
import { Observable, of } from "rxjs";
import { catchError, map } from "rxjs/operators";

import { Chore, ChoreEditForm } from "../types/chore.type";

@Injectable({
  providedIn: "root",
})
export class ChoreRepository {
  constructor(private _httpService: HttpService) {}

  public getChores(dateStart: Date, dateEnd: Date): Observable<Chore[] | null> {
    const rangeDateStart = getDateStr(dateStart);
    const rangeDateEnd = getDateStr(dateEnd);
    return this._httpService
      .get<Response<Chore[]>>("/daily/chores", {
        params: {
          rangeDateStart,
          rangeDateEnd,
        },
      })
      .pipe(
        map((response) => response.result),
        map((result) =>
          plainToClass(Chore, result?.data || [], {
            excludeExtraneousValues: true,
          }),
        ),
        // TODO: Better way to integrate with data-loader component
        catchError(() => of(null)),
      );
  }

  public createChore(data: ChoreEditForm): Observable<Chore> {
    return this._httpService
      .post<Response<Chore>>("/daily/chores", {
        body: classToPlain(data),
      })
      .pipe(
        map((response) => response.result),
        map((result) =>
          plainToClass(Chore, result?.data, {
            excludeExtraneousValues: true,
          }),
        ),
      );
  }

  public updateChore(choreId: string, data: ChoreEditForm): Observable<Chore> {
    return this._httpService
      .put<Response<Chore>>(`/daily/chores/${choreId}`, {
        body: classToPlain(data),
      })
      .pipe(
        map((response) => response.result),
        map((result) =>
          plainToClass(Chore, result?.data, {
            excludeExtraneousValues: true,
          }),
        ),
      );
  }

  public updateChoreStatus(
    choreId: string,
    date: string,
    status: ChoreStatus,
  ): Observable<void> {
    return this._httpService.put(`/daily/chores/${choreId}/status`, {
      body: { date, status },
    });
  }
}
