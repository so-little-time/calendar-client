import { BaseComponent } from "@/core/components/base-components/base.component";
import { ComponentService } from "@/core/components/base-components/component.service";
import { ChoreStatus } from "@/daily/const/chore.const";
import { ChoreRepository } from "@/daily/repositories/chore.repository";
import { Chore } from "@/daily/types/chore.type";
import { DayInYear } from "@/daily/types/date-time.type";
import { ReloadableData } from "@/shared/components/data-loader/data-loader.type";
import {
  cloneDate,
  getAbsCurrDate,
  getDayInWeekIndex,
} from "@/shared/helpers/date-time.helper";
import { DialogService } from "@/shared/modules/dialog/dialog.service";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from "@angular/core";

import { ChoreEditComponent } from "./components/chore-edit/chore-edit.component";
import { DAYS_IN_WEEK } from "./const/date-time.const";
import { EUpdateChoreStatusPayload } from "./types/event.type";

@Component({
  selector: "app-daily",
  templateUrl: "./daily.component.html",
  styleUrls: ["./daily.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DailyComponent extends BaseComponent {
  currentDate!: Date;
  viewDate!: Date;
  viewWeekMondayDate!: Date;
  viewWeekSundayDate!: Date;
  viewWeekDays: DayInYear[] = [];
  chores: ReloadableData<Chore[]> = undefined;

  constructor(
    protected componentService: ComponentService,
    private _choreRepository: ChoreRepository,
    private _dialogService: DialogService,
    private _cdr: ChangeDetectorRef,
  ) {
    super(componentService);
  }

  protected preInit(): void {
    this.currentDate = getAbsCurrDate();
    this.viewDate = cloneDate(this.currentDate);
  }

  protected postInit(): void {
    this.navigateWeek(this.currentDate);
  }

  public navigateWeek(viewDate: Date): void {
    const weekDays = this._generateWeek(viewDate);

    this.viewWeekMondayDate = weekDays[0].date;
    this.viewWeekSundayDate = weekDays[DAYS_IN_WEEK.length - 1].date;
    this.viewDate = cloneDate(viewDate);
    this.viewWeekDays = weekDays;
    this.fetchWeekChores();
  }

  public handleCheckBoxToggled(payload: EUpdateChoreStatusPayload): void {
    const { choreId, date, checked } = payload;
    this.pipeSession(
      this._choreRepository.updateChoreStatus(
        choreId,
        date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate(),
        checked ? ChoreStatus.Done : ChoreStatus.Pending,
      ),
    ).subscribe(() => undefined);
  }

  public handleEditChore(chore?: Chore): void {
    const dialogRef = this._dialogService.open<
      ChoreEditComponent,
      Chore,
      Chore
    >(ChoreEditComponent, {
      data: chore,
    });
    this.pipeOnce(dialogRef.afterClosed).subscribe((chore) => {
      if (chore) {
        this.fetchWeekChores();
      }
    });
  }

  public fetchWeekChores(): void {
    this.chores = undefined;
    this.resetSession();
    this.pipeNext(
      this._choreRepository.getChores(
        this.viewWeekMondayDate,
        this.viewWeekSundayDate,
      ),
    ).subscribe((chores) => {
      this.chores = chores;
      this._cdr.markForCheck();
    });
  }

  private _generateWeek(viewDate: Date): DayInYear[] {
    const clonedViewDate = cloneDate(viewDate);
    const mondayDate = new Date(
      clonedViewDate.setDate(
        clonedViewDate.getDate() - getDayInWeekIndex(clonedViewDate),
      ),
    );

    // Populate days in view week
    const weekDays: DayInYear[] = [];
    for (let i = 0; i < DAYS_IN_WEEK.length; i++) {
      const clonedMondayDate = cloneDate(mondayDate);
      const dayInWeekDate = new Date(
        clonedMondayDate.setDate(clonedMondayDate.getDate() + i),
      );
      weekDays.push(new DayInYear(dayInWeekDate));
    }

    return weekDays;
  }
}
