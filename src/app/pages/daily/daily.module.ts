import { SharedModule } from "@/shared/shared.module";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { ChoreEditComponent } from "./components/chore-edit/chore-edit.component";
import { WeekNavigationBarComponent } from "./components/week-navigation-bar/week-navigation-bar.component";
import { WeekTableComponent } from "./components/week-table/week-table.component";
import { DailyRoutingModule } from "./daily-routing.module";
import { DailyComponent } from "./daily.component";

@NgModule({
  declarations: [
    DailyComponent,
    WeekNavigationBarComponent,
    WeekTableComponent,
    ChoreEditComponent,
  ],
  imports: [CommonModule, DailyRoutingModule, SharedModule],
})
export class DailyModule {}
