import {
  getAbsDateFromString,
  getDateStrOrNull,
  isSameDate,
} from "@/shared/helpers/date-time.helper";
import { BaseModel } from "@/shared/types/base.type";
import { Expose, Transform, Type, plainToClass } from "class-transformer";

export class Chore extends BaseModel {
  @Expose({ name: "name" })
  public name = "";

  @Expose({ name: "startDate" })
  @Transform(
    ({ value }) => {
      if (!value) {
        return null;
      }
      return getAbsDateFromString(value);
    },
    { toClassOnly: true },
  )
  public startDate?: Date;

  @Expose({ name: "endDate" })
  @Transform(
    ({ value }) => {
      if (!value) {
        return null;
      }
      return getAbsDateFromString(value);
    },
    { toClassOnly: true },
  )
  public endDate?: Date;

  @Expose({ name: "choreDates" })
  @Type(() => ChoreDate)
  @Transform(({ value }) => value || [], { toClassOnly: true })
  public choreDates!: ChoreDate[];

  public isDoneIn(date: Date): boolean {
    return this.choreDates.some((choreDate) =>
      isSameDate(choreDate.value, date),
    );
  }
}

export class ChoreDate extends BaseModel {
  @Expose({ name: "value" })
  @Transform(
    ({ value }) => {
      return getAbsDateFromString(value);
    },
    { toClassOnly: true },
  )
  public value!: Date;
}

export class ChoreEditForm {
  @Expose({ name: "name" })
  name: string = "";

  @Expose({ name: "startDate" })
  @Transform(({ value }) => getDateStrOrNull(value))
  startDate: string | null = null;

  @Expose({ name: "endDate" })
  @Transform(({ value }) => getDateStrOrNull(value))
  endDate: string | null = null;

  public fromChore(chore?: Chore): ChoreEditForm {
    if (!chore) {
      return this;
    }

    return plainToClass(ChoreEditForm, chore, {
      excludeExtraneousValues: true,
    });
  }
}
