import {
  getDayInWeekIndex,
  isSameDate,
} from "@/shared/helpers/date-time.helper";

import { DAYS_IN_WEEK } from "../const/date-time.const";

export interface DayInWeek {
  index: number;
  abbrv: string;
  fullName: string;
}

export class DayInYear {
  dayInWeek: DayInWeek;
  date: Date;
  displayDate: string;

  constructor(date: Date) {
    const dayInWeekIndex = getDayInWeekIndex(date);
    this.dayInWeek = {
      ...DAYS_IN_WEEK[dayInWeekIndex],
      index: dayInWeekIndex,
    };
    this.date = date;
    this.displayDate = date.getDate() + "/" + (date.getMonth() + 1);
  }

  isDate(date: Date) {
    return isSameDate(this.date, date);
  }
}
