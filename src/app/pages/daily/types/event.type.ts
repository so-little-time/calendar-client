export interface EUpdateChoreStatusPayload {
  choreId: string;
  date: Date;
  checked: boolean;
}
