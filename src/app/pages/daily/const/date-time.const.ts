export const DAYS_IN_WEEK = [
  { abbrv: "Mon", fullName: "Monday" },
  { abbrv: "Tue", fullName: "Tuesday" },
  { abbrv: "Wed", fullName: "Wednesday" },
  { abbrv: "Thu", fullName: "Thursday" },
  { abbrv: "Fri", fullName: "Friday" },
  { abbrv: "Sat", fullName: "Saturday" },
  { abbrv: "Sun", fullName: "Sunday" },
];
