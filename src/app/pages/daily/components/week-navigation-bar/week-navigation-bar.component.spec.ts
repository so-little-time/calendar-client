import { ComponentFixture, TestBed } from "@angular/core/testing";

import { WeekNavigationBarComponent } from "./week-navigation-bar.component";

describe("WeekNavigationBarComponent", () => {
  let component: WeekNavigationBarComponent;
  let fixture: ComponentFixture<WeekNavigationBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WeekNavigationBarComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WeekNavigationBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
