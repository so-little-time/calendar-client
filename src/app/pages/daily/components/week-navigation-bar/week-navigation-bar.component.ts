import { BaseComponent } from "@/core/components/base-components/base.component";
import { ComponentService } from "@/core/components/base-components/component.service";
import { isSameDate } from "@/shared/helpers/date-time.helper";
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from "@angular/core";

@Component({
  selector: "app-week-navigation-bar",
  templateUrl: "./week-navigation-bar.component.html",
  styleUrls: ["./week-navigation-bar.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WeekNavigationBarComponent extends BaseComponent {
  @Input() public viewDate!: Date;
  @Input() public currentDate!: Date;
  @Output() public weekNavigationBarNavigated = new EventEmitter<Date>();
  @Output() public weekNavigationBarAddChore = new EventEmitter<undefined>();
  public isSameDate = isSameDate;

  constructor(protected componentService: ComponentService) {
    super(componentService);
  }

  public navigateWeekByStep(step = 0): void {
    const clonedViewDate = new Date(this.viewDate);
    clonedViewDate.setDate(clonedViewDate.getDate() + step * 7);
    this.weekNavigationBarNavigated.emit(clonedViewDate);
  }
}
