import { BaseComponent } from "@/core/components/base-components/base.component";
import { ComponentService } from "@/core/components/base-components/component.service";
import { ChoreRepository } from "@/daily/repositories/chore.repository";
import { Chore, ChoreEditForm } from "@/daily/types/chore.type";
import { DIALOG_DATA } from "@/shared/modules/dialog/dialog.const";
import { DialogRef } from "@/shared/modules/dialog/dialog.type";
import { ChangeDetectionStrategy, Component, Inject } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { plainToClass } from "class-transformer";
import { Observable } from "rxjs";

@Component({
  selector: "app-chore-edit",
  templateUrl: "./chore-edit.component.html",
  styleUrls: ["./chore-edit.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChoreEditComponent extends BaseComponent {
  public choreEditForm!: FormGroup;

  constructor(
    @Inject(DIALOG_DATA) public chore: Chore | undefined,
    public dialogRef: DialogRef<ChoreEditComponent>,
    protected componentService: ComponentService,
    private _formBuilder: FormBuilder,
    private _choreRepository: ChoreRepository,
  ) {
    super(componentService);
  }

  protected preInit(): void {
    const formDefaultValue = new ChoreEditForm().fromChore(this.chore);

    this.choreEditForm = this._formBuilder.group({
      name: ["", [Validators.required]],
      startDate: [null],
      endDate: [null],
    });
    this.choreEditForm.setValue(formDefaultValue);
  }

  public handleSubmit(): void {
    if (!this.choreEditForm.valid) {
      return;
    }

    // TODO: Need better typesafe, class-transform logic. Still feel redundant with operation in repository
    const choreData = plainToClass(ChoreEditForm, this.choreEditForm.value);
    this.pipeOnce(this._selectEditOperation(choreData)).subscribe((chore) =>
      this.dialogRef.close(chore),
    );
  }

  private _selectEditOperation(choreData: ChoreEditForm): Observable<Chore> {
    if (this.chore) {
      return this._choreRepository.updateChore(this.chore.id, choreData);
    }

    return this._choreRepository.createChore(choreData);
  }
}
