import { BaseComponent } from "@/core/components/base-components/base.component";
import { ComponentService } from "@/core/components/base-components/component.service";
import { Chore } from "@/daily/types/chore.type";
import { DayInYear } from "@/daily/types/date-time.type";
import { EUpdateChoreStatusPayload } from "@/daily/types/event.type";
import { ReloadableData } from "@/shared/components/data-loader/data-loader.type";
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from "@angular/core";

@Component({
  selector: "app-week-table",
  templateUrl: "./week-table.component.html",
  styleUrls: ["./week-table.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WeekTableComponent extends BaseComponent {
  @Input() public viewWeekDays: DayInYear[] = [];
  @Input() public currentDate!: Date;
  @Input() public chores: ReloadableData<Chore[]> = undefined;
  @Output() public weekTableUpdateChoreStatus =
    new EventEmitter<EUpdateChoreStatusPayload>();
  @Output() public weekTableUpdateChore = new EventEmitter<Chore>();
  @Output() public weekTableReload = new EventEmitter<void>();

  constructor(protected componentService: ComponentService) {
    super(componentService);
  }

  public updateChoreStatus(
    choreId: string,
    date: Date,
    checked: boolean,
  ): void {
    this.weekTableUpdateChoreStatus.emit({
      choreId,
      date,
      checked,
    });
  }
}
