export function convertDayInWeekIndex(index: number): number {
  return index === 0 ? 6 : index - 1;
}

export function getCurrDayInWeekIndex(): number {
  const date = new Date();
  return getDayInWeekIndex(date);
}

export function getDayInWeekIndex(date: Date): number {
  let index = date.getDay();
  return convertDayInWeekIndex(index);
}

export function getAbsDate(date: Date): Date {
  const absDate = new Date(date.getFullYear(), date.getMonth(), date.getDate());
  return absDate;
}

export function getAbsDateFromString(value: string): Date {
  let absDate = new Date(value);
  absDate = new Date(
    absDate.getFullYear(),
    absDate.getMonth(),
    absDate.getDate(),
  );
  return absDate;
}

export function getAbsCurrDate(): Date {
  let currentDate = new Date();
  return getAbsDate(currentDate);
}

export function isSameDate(date: Date, targetDate: Date): boolean {
  return (
    date.getFullYear() === targetDate.getFullYear() &&
    date.getMonth() === targetDate.getMonth() &&
    date.getDate() === targetDate.getDate()
  );
}

export function isSameMonth(date: Date, targetDate: Date): boolean {
  return (
    date.getFullYear() === targetDate.getFullYear() &&
    date.getMonth() === targetDate.getMonth()
  );
}

export function getDateStr(date: Date): string {
  const formatMonth = ("0" + (date.getMonth() + 1)).slice(-2);
  const formatDate = ("0" + date.getDate()).slice(-2);
  return date.getFullYear() + "-" + formatMonth + "-" + formatDate;
}

export function getDateStrOrNull(value?: Date | string | null): string | null {
  if (!value) {
    return null;
  }

  if (value instanceof Date) {
    return getDateStr(value);
  }
  return value;
}

export function cloneDate(date: Date): Date {
  const clonedDate = new Date(date.valueOf());
  return clonedDate;
}
