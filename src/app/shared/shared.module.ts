import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { CheckBoxComponent } from "./components/check-box/check-box.component";
import { DataLoaderComponent } from "./components/data-loader/data-loader.component";
import { InputTextComponent } from "./components/inputs/input-text/input-text.component";
import { DatePickerModule } from "./modules/date-picker/date-picker.module";
import { DialogModule } from "./modules/dialog/dialog.module";

@NgModule({
  declarations: [CheckBoxComponent, DataLoaderComponent, InputTextComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DialogModule,
    DatePickerModule,
    CheckBoxComponent,
    DataLoaderComponent,
    InputTextComponent,
  ],
})
export class SharedModule {}
