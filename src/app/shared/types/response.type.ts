import { Expose } from "class-transformer";

export class Result<T> {
  data!: T;
}

export class Pagination {
  @Expose({ name: "page" })
  page: number = 0;

  @Expose({ name: "perPage" })
  perPage: number = 0;

  @Expose({ name: "perPage" })
  lastPage: number = 0;

  @Expose({ name: "total" })
  total: number = 0;
}

export class ResultPagination<T> {
  data!: T[];

  pagination!: Pagination;
}

// Response
export class Response<T = any> {
  code!: number;

  message?: string;

  result: Result<T> | null = null;
}

// Response with Pagination
export class ResponsePagination<T> extends Response {
  result!: ResultPagination<T>;
}
