import { Exclude, Expose } from "class-transformer";

export class BaseModel {
  @Exclude()
  public type: Function;

  @Expose({ name: "id" })
  public id!: string;

  constructor(type: Function) {
    this.type = type;
  }
}
