import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from "@angular/core";

@Component({
  selector: "app-check-box",
  templateUrl: "./check-box.component.html",
  styleUrls: ["./check-box.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CheckBoxComponent {
  private _checkedValue!: boolean;

  public set checkedValue(value: boolean) {
    this._checkedValue = value;
    this.checkBoxToggled.emit(value);
  }

  public get checkedValue(): boolean {
    return this._checkedValue;
  }

  @Input() set isChecked(value: boolean) {
    this._checkedValue = value;
  }

  @Input() isDisabled = false;

  @Output() checkBoxToggled = new EventEmitter<boolean>();

  constructor() {}

  public toggle(): void {
    this.checkedValue = !this.checkedValue;
  }
}
