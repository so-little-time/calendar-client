export type ReloadableData<T> = T | undefined | null;
