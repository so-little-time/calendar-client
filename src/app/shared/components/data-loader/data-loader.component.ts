import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from "@angular/core";

import { ReloadableData } from "./data-loader.type";

@Component({
  selector: "app-data-loader",
  templateUrl: "./data-loader.component.html",
  styleUrls: ["./data-loader.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DataLoaderComponent {
  /**
   * @state `undefined`: not loaded
   * @state `null`: load failed
   * @state other: load success
   */
  @Input() data: ReloadableData<any> = undefined;
  @Output() dataLoaderComponentReload = new EventEmitter<void>();

  constructor() {}
}
