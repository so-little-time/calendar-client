import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  OnInit,
  Output,
} from "@angular/core";

@Component({
  selector: "app-search-bar",
  templateUrl: "./search-bar.component.html",
  styleUrls: ["./search-bar.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchBarComponent {
  @Output() searchBarSubmitted = new EventEmitter<string>();
  @Output() searchBarChanged = new EventEmitter<string>();
  private _searchValue = "";
  public set searchValue(data: string) {
    this._searchValue = data;
    this.searchBarChanged.emit(data);
  }
  public get searchValue(): string {
    return this._searchValue;
  }

  constructor() {}

  public clear(): void {
    this._searchValue = "";
  }

  public onSubmit(): void {
    this.searchBarSubmitted.emit(this.searchValue);
  }
}
