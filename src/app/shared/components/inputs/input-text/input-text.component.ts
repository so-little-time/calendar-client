import {
  ChangeDetectionStrategy,
  Component,
  Input,
  Optional,
  Self,
} from "@angular/core";
import { ControlValueAccessor, NgControl } from "@angular/forms";

import { InputType } from "./input-text.type";

@Component({
  selector: "app-input-text",
  templateUrl: "./input-text.component.html",
  styleUrls: ["./input-text.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InputTextComponent implements ControlValueAccessor {
  @Input() public placeholder = "";
  @Input() public disabled = false;
  @Input() public type: InputType = "text";

  private _value = "";
  public get value(): string {
    return this._value;
  }

  public set value(value: string) {
    if (this._value === value) {
      return;
    }

    this._value = value;
    this.onChange(value);
  }

  constructor(@Self() @Optional() private _ngControl: NgControl) {
    if (this._ngControl) {
      this._ngControl.valueAccessor = this;
    }
  }

  /**
   * @override
   */
  public writeValue(value: string): void {
    this._value = value;
  }

  /**
   * @override
   */
  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  /**
   * @override
   */
  public registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  /**
   * @override
   */
  public setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  public onChange = (_value: string): void => {};
  public onTouched = (): void => {};
}
