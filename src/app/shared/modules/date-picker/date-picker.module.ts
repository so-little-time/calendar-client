import { OverlayModule } from "@angular/cdk/overlay";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { CalendarDailyComponent } from "./calendar-daily/calendar-daily.component";
import { CalendarMenuComponent } from "./calendar-menu/calendar-menu.component";
import { CalendarYearlyComponent } from "./calendar-yearly/calendar-yearly.component";
import { CalendarComponent } from "./calendar/calendar.component";
import { DatePickerComponent } from "./date-picker/date-picker.component";

@NgModule({
  declarations: [
    DatePickerComponent,
    CalendarComponent,
    CalendarDailyComponent,
    CalendarYearlyComponent,
    CalendarMenuComponent,
  ],
  imports: [CommonModule, OverlayModule],
  exports: [DatePickerComponent],
})
export class DatePickerModule {}
