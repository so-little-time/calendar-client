export const DAYS_IN_WEEK = [
  { name: "Monday", twoLetters: "Mo" },
  { name: "Tuesday", twoLetters: "Tu" },
  { name: "Wednesday", twoLetters: "We" },
  { name: "Thursday", twoLetters: "Th" },
  { name: "Friday", twoLetters: "Fr" },
  { name: "Saturday", twoLetters: "Sa" },
  { name: "Sunday", twoLetters: "Su" },
];

export const MONTHS = [
  { name: "January", threeLetters: "Jan" },
  { name: "Febuary", threeLetters: "Feb" },
  { name: "March", threeLetters: "Mar" },
  { name: "April", threeLetters: "Apr" },
  { name: "May", threeLetters: "May" },
  { name: "June", threeLetters: "Jun" },
  { name: "July", threeLetters: "Jul" },
  { name: "August", threeLetters: "Aug" },
  { name: "September", threeLetters: "Sep" },
  { name: "October", threeLetters: "Oct" },
  { name: "November", threeLetters: "Nov" },
  { name: "December", threeLetters: "Dec" },
];

export enum CalendarState {
  Daily = "Daily",
  Yearly = "Yearly",
}

export const YEAR_RANGE_ROW_NUM = 6;
export const YEAR_RANGE_COLUMN_NUM = 4;
export const YEAR_RANGE_ITEM_NUM = YEAR_RANGE_ROW_NUM * YEAR_RANGE_COLUMN_NUM;
