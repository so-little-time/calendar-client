import { ChangeDetectionStrategy, Component, Input } from "@angular/core";

@Component({
  selector: "app-date-picker",
  templateUrl: "./date-picker.component.html",
  styleUrls: ["./date-picker.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DatePickerComponent {
  @Input() public placeholder = "dd/mm/yyyy";
  @Input() public disabled = false;
  public isCalendarOpened = false;

  constructor() {}

  public toggleCalendar(): void {
    this.isCalendarOpened = !this.isCalendarOpened;
  }
}
