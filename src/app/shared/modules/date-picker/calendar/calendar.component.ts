import { ChangeDetectionStrategy, Component } from "@angular/core";

import { CalendarState } from "../date-picker.const";
import { DatePickerService } from "../date-picker.service";

@Component({
  selector: "app-calendar",
  templateUrl: "./calendar.component.html",
  styleUrls: ["./calendar.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePickerService],
})
// TODO: Add control-value-accessor
export class CalendarComponent {
  public CalendarState = CalendarState;

  constructor(public datePickerService: DatePickerService) {}
}
