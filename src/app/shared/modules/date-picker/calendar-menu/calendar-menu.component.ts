import { isSameMonth } from "@/shared/helpers/date-time.helper";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from "@angular/core";
import { skip } from "rxjs/operators";

import {
  CalendarState,
  MONTHS,
  YEAR_RANGE_ITEM_NUM,
} from "../date-picker.const";
import { DatePickerService } from "../date-picker.service";

@Component({
  selector: "app-calendar-menu",
  templateUrl: "./calendar-menu.component.html",
  styleUrls: ["./calendar-menu.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CalendarMenuComponent implements OnInit {
  public MONTHS = MONTHS;
  public CalendarState = CalendarState;

  ngOnInit(): void {
    const { calendarState$ } = this.datePickerService;
    calendarState$.pipe(skip(1)).subscribe(() => {
      this._changeDetectorRef.markForCheck();
    });
  }

  public get periodText(): string {
    const { calendarState } = this.datePickerService;
    if (calendarState === CalendarState.Daily) {
      return (
        MONTHS[this.datePickerService.viewMonth].name +
        " " +
        this.datePickerService.viewYear
      );
    } else {
      const { minViewYearRange, maxViewYearRange } = this.datePickerService;
      return minViewYearRange + " - " + maxViewYearRange;
    }
  }

  constructor(
    public datePickerService: DatePickerService,
    private _changeDetectorRef: ChangeDetectorRef,
  ) {}

  public iterateCalendarState(): void {
    const { calendarState } = this.datePickerService;
    if (calendarState === CalendarState.Daily) {
      this.datePickerService.calendarState = CalendarState.Yearly;
    } else {
      this.datePickerService.calendarState = CalendarState.Daily;
    }
  }

  public isSamePeriod(): boolean {
    const { calendarState } = this.datePickerService;
    if (calendarState === CalendarState.Daily) {
      const { currentDate, fdimDate } = this.datePickerService;
      return isSameMonth(fdimDate, currentDate);
    } else {
      const { currentYear, minViewYearRange, maxViewYearRange } =
        this.datePickerService;
      return minViewYearRange <= currentYear && currentYear <= maxViewYearRange;
    }
  }

  public navigatePeriodReset(): void {
    const { calendarState, currentDate, currentYear } = this.datePickerService;
    if (calendarState === CalendarState.Daily) {
      this.datePickerService.fdimDate = currentDate;
    } else {
      this.datePickerService.viewYearRangeMarker = currentYear;
    }
  }

  public navigatePeriodByStep(step = 0): void {
    const { calendarState, viewYear, viewMonth, viewYearRangeMarker } =
      this.datePickerService;
    if (calendarState === CalendarState.Daily) {
      const nextMonth = viewMonth + step;
      this.datePickerService.setFdimDatePartially(viewYear, nextMonth);
    } else {
      const nextYear = viewYearRangeMarker + step * YEAR_RANGE_ITEM_NUM;
      this.datePickerService.viewYearRangeMarker = nextYear;
    }
  }
}
