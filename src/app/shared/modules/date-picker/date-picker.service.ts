import {
  cloneDate,
  convertDayInWeekIndex,
} from "@/shared/helpers/date-time.helper";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";

import { CalendarState } from "./date-picker.const";

@Injectable()
export class DatePickerService {
  // private `currentDate`
  private _currentDate: Date;
  public get currentDate(): Date {
    return this._currentDate;
  }

  // private `calendarState`
  private _calendarState$: BehaviorSubject<CalendarState> =
    new BehaviorSubject<CalendarState>(CalendarState.Daily);
  public get calendarState(): CalendarState {
    return this._calendarState$.value;
  }
  public get calendarState$(): Observable<CalendarState> {
    return this._calendarState$.asObservable();
  }
  public set calendarState(state: CalendarState) {
    this.viewYearRangeMarker = this.viewYear;
    this._calendarState$.next(state);
  }

  // private `viewDate`
  private _fdimDate$: BehaviorSubject<Date> = new BehaviorSubject<Date>(
    new Date(),
  );
  public get fdimDate(): Date {
    return this._fdimDate$.value;
  }
  public get fdimDate$(): Observable<Date> {
    return this._fdimDate$.asObservable();
  }
  public set fdimDate(date: Date) {
    const clonedDate = cloneDate(date);
    const year = clonedDate.getFullYear();
    const month = clonedDate.getMonth();
    const firstDateInMonth = new Date(year, month);

    this._viewYear = firstDateInMonth.getFullYear();
    this._viewMonth = firstDateInMonth.getMonth();
    this._offset = convertDayInWeekIndex(firstDateInMonth.getDay());
    this._fdimDate$.next(firstDateInMonth);
    this.viewYearRangeMarker = this.viewYear;
  }

  // private `viewYearRangeMarker`
  private _viewYearRangeMarker$: BehaviorSubject<number> =
    new BehaviorSubject<number>(0);
  public get viewYearRangeMarker(): number {
    return this._viewYearRangeMarker$.value;
  }
  public get viewYearRangeMarker$(): Observable<number> {
    return this._viewYearRangeMarker$.asObservable();
  }
  public set viewYearRangeMarker(year: number) {
    this._viewYearRangeMarker$.next(year);
  }

  // private `currentYear`
  private _currentYear!: number;
  public get currentYear(): number {
    return this._currentYear;
  }

  // private `currentMonth`
  private _currentMonth!: number;
  public get currentMonth(): number {
    return this._currentMonth;
  }

  // private `viewYear`
  private _viewYear!: number;
  public get viewYear(): number {
    return this._viewYear;
  }

  // private `viewMonth`
  private _viewMonth!: number;
  public get viewMonth(): number {
    return this._viewMonth;
  }

  // private `offset`
  private _offset!: number;
  public get offset(): number {
    return this._offset;
  }

  // private `minViewYearRange`
  private _minViewYearRange!: number;
  public get minViewYearRange(): number {
    return this._minViewYearRange;
  }

  // private `maxViewYearRange`
  private _maxViewYearRange!: number;
  public get maxViewYearRange(): number {
    return this._maxViewYearRange;
  }

  constructor() {
    this.calendarState = CalendarState.Daily;
    this._currentDate = new Date();
    this._currentYear = this.currentDate.getFullYear();
    this._currentMonth = this.currentDate.getMonth();
    this.setFdimDatePartially(this.currentYear, this.currentMonth);
  }

  public setFdimDatePartially(year: number, month: number): void {
    this.fdimDate = new Date(year, month);
  }

  public setViewYearRange(years: number[][]): void {
    const lastRowIndex = years.length - 1;
    const lastCellIndex = years[lastRowIndex]?.length - 1;
    this._minViewYearRange = years[0]?.[0];
    this._maxViewYearRange = years[lastRowIndex]?.[lastCellIndex];
  }

  public reset(): void {
    this.calendarState = CalendarState.Daily;
    this.fdimDate = cloneDate(this.currentDate);
  }
}
