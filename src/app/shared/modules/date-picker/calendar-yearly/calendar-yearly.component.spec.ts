import { ComponentFixture, TestBed } from "@angular/core/testing";

import { CalendarYearlyComponent } from "./calendar-yearly.component";

describe("CalendarYearlyComponent", () => {
  let component: CalendarYearlyComponent;
  let fixture: ComponentFixture<CalendarYearlyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CalendarYearlyComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarYearlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
