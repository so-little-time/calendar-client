import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from "@angular/core";
import { skip } from "rxjs/operators";

import {
  CalendarState,
  YEAR_RANGE_COLUMN_NUM,
  YEAR_RANGE_ITEM_NUM,
  YEAR_RANGE_ROW_NUM,
} from "../date-picker.const";
import { DatePickerService } from "../date-picker.service";

@Component({
  selector: "app-calendar-yearly",
  templateUrl: "./calendar-yearly.component.html",
  styleUrls: ["./calendar-yearly.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CalendarYearlyComponent implements OnInit {
  public years!: number[][];

  constructor(
    public datePickerService: DatePickerService,
    private _changeDetectorRef: ChangeDetectorRef,
  ) {}

  ngOnInit(): void {
    const { viewYearRangeMarker, viewYearRangeMarker$ } =
      this.datePickerService;
    this.years = this._getYears(viewYearRangeMarker);
    viewYearRangeMarker$.pipe(skip(1)).subscribe((year) => {
      this.years = this._getYears(year);
      this._changeDetectorRef.markForCheck();
    });
  }

  public selectYear(year: number): void {
    this.datePickerService.setFdimDatePartially(
      year,
      this.datePickerService.viewMonth,
    );
    this.datePickerService.calendarState = CalendarState.Daily;
  }

  private _getYears(yearMarker: number): number[][] {
    const minYear =
      Math.floor(yearMarker / YEAR_RANGE_ITEM_NUM) * YEAR_RANGE_ITEM_NUM;
    let i, j;
    const years: number[][] = [];
    for (i = 0; i < YEAR_RANGE_ROW_NUM; i++) {
      years.push([]);
      for (j = 0; j < YEAR_RANGE_COLUMN_NUM; j++) {
        years[i].push(minYear + i * YEAR_RANGE_COLUMN_NUM + j);
      }
    }
    this.datePickerService.setViewYearRange(years);
    return years;
  }
}
