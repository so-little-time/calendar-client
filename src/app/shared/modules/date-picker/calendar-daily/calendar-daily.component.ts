import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from "@angular/core";
import { skip } from "rxjs/operators";

import { DAYS_IN_WEEK } from "../date-picker.const";
import { DatePickerService } from "../date-picker.service";

@Component({
  selector: "app-calendar-daily",
  templateUrl: "./calendar-daily.component.html",
  styleUrls: ["./calendar-daily.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CalendarDailyComponent implements OnInit {
  public DAYS_IN_WEEK = DAYS_IN_WEEK;

  constructor(
    private _datePickerService: DatePickerService,
    private _changeDetectorRef: ChangeDetectorRef,
  ) {}

  ngOnInit(): void {
    const { fdimDate$: viewDate$ } = this._datePickerService;
    viewDate$
      .pipe(skip(1))
      .subscribe(() => this._changeDetectorRef.detectChanges());
  }

  public shouldShowDay(rowIndex: number, columnIndex: number): boolean {
    const { viewYear, viewMonth } = this._datePickerService;
    const dayInMonth = this.getDayInMonth(rowIndex, columnIndex);
    const date = new Date(viewYear, viewMonth, dayInMonth);

    return dayInMonth > 0 && date.getMonth() === viewMonth;
  }

  public isCurrentDay(rowIndex: number, columnIndex: number): boolean {
    const { currentDate, currentYear, currentMonth, viewYear, viewMonth } =
      this._datePickerService;
    const dayInMonth = this.getDayInMonth(rowIndex, columnIndex);
    return (
      viewMonth === currentMonth &&
      dayInMonth === currentDate.getDate() &&
      viewYear === currentYear
    );
  }

  public getDayInMonth(rowIndex: number, columnIndex: number): number {
    return rowIndex * 7 + columnIndex + 1 - this._datePickerService.offset;
  }
}
