import { InjectionToken } from "@angular/core";

import { DialogConfig } from "./dialog.type";

export const DIALOG_DATA = new InjectionToken<any>("DialogData");

export const DIALOG_DEFAULT_CONFIG = new InjectionToken<DialogConfig>(
  "DialogDefaultConfig",
);
