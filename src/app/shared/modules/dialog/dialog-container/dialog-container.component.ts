import { CdkPortalOutlet, ComponentPortal } from "@angular/cdk/portal";
import {
  ChangeDetectionStrategy,
  Component,
  ComponentRef,
  OnInit,
  ViewChild,
} from "@angular/core";

@Component({
  selector: "app-dialog-container",
  templateUrl: "./dialog-container.component.html",
  styleUrls: ["./dialog-container.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DialogContainerComponent {
  @ViewChild(CdkPortalOutlet, { static: true }) _portalOutlet!: CdkPortalOutlet;

  constructor() {}

  public attachComponentPortal<T>(portal: ComponentPortal<T>): ComponentRef<T> {
    return this._portalOutlet.attachComponentPortal<T>(portal);
  }
}
