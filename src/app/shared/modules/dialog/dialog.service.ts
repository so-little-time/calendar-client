import { Overlay, OverlayConfig, OverlayRef } from "@angular/cdk/overlay";
import { ComponentPortal, ComponentType } from "@angular/cdk/portal";
import { Inject, Injectable, Injector, StaticProvider } from "@angular/core";

import { DialogContainerComponent } from "./dialog-container/dialog-container.component";
import { DIALOG_DATA, DIALOG_DEFAULT_CONFIG } from "./dialog.const";
import { DialogConfig, DialogRef } from "./dialog.type";

// TODO: Add comments for important methods
// TODO: Add `closeAll` method
@Injectable({
  providedIn: "root",
})
export class DialogService {
  constructor(
    @Inject(DIALOG_DEFAULT_CONFIG) private _dialogDefaultConfig: DialogConfig,
    private _overlay: Overlay,
    private _injector: Injector,
  ) {}

  public open<T, D = any, R = any>(
    component: ComponentType<T>,
    config?: DialogConfig<D>,
  ) {
    const dialogConfig = { ...this._dialogDefaultConfig, ...config };

    const overlayRef = this._createOverlayRef(dialogConfig);

    const dialogContainer = this._attachDialogContainer(overlayRef);

    const dialogRef = this._attachDialogContent<T, R>(
      component,
      dialogContainer,
      overlayRef,
      dialogConfig,
    );

    return dialogRef;
  }

  private _createOverlayRef(config: DialogConfig): OverlayRef {
    const overlayConfig = this._getOverlayConfig(config);
    return this._overlay.create(overlayConfig);
  }

  private _getOverlayConfig(config: DialogConfig): OverlayConfig {
    const positionStrategy = this._overlay
      .position()
      .global()
      .centerHorizontally()
      .centerVertically();

    const overlayConfig = new OverlayConfig({
      positionStrategy,
      hasBackdrop: config.hasBackdrop,
      panelClass: config.panelClass,
      disposeOnNavigation: config.closeOnNavigation,
    });

    if (config.backdropClass) {
      overlayConfig.backdropClass = config.backdropClass;
    }

    return overlayConfig;
  }

  private _attachDialogContainer(
    overlayRef: OverlayRef,
  ): DialogContainerComponent {
    const containerPortal = new ComponentPortal(DialogContainerComponent);
    const containerRef =
      overlayRef.attach<DialogContainerComponent>(containerPortal);

    return containerRef.instance;
  }

  private _attachDialogContent<T, R>(
    component: ComponentType<T>,
    dialogContainer: DialogContainerComponent,
    overlayRef: OverlayRef,
    config: DialogConfig,
  ): DialogRef<T, R> {
    const dialogRef = new DialogRef<T, R>(overlayRef);

    const injector = this._createInjector<T>(config, dialogRef);
    const contentRef = dialogContainer.attachComponentPortal(
      new ComponentPortal(component, null, injector),
    );
    dialogRef.componentInstance = contentRef.instance;

    return dialogRef;
  }

  private _createInjector<T>(
    config: DialogConfig,
    dialogRef: DialogRef<T>,
  ): Injector {
    const providers: StaticProvider[] = [
      {
        provide: DIALOG_DATA,
        useValue: config.data,
      },
      {
        provide: DialogRef,
        useValue: dialogRef,
      },
    ];

    return Injector.create({ parent: this._injector, providers });
  }
}
