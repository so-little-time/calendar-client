import { OverlayRef } from "@angular/cdk/overlay";
import { Subject } from "rxjs";

export class DialogConfig<D = any> {
  public data?: D;

  public hasBackdrop?: boolean = true;

  public panelClass?: string | string[] = "";

  public backdropClass?: string | string[] = "";

  public closeOnNavigation?: boolean = true;
}

export class DialogRef<T, R = any> {
  public componentInstance!: T;

  public afterClosed = new Subject<R | undefined>();

  private _overlayRef: OverlayRef;

  private _result?: R;

  constructor(overlayRef: OverlayRef) {
    overlayRef.detachments().subscribe(() => {
      this.afterClosed.next(this._result);
    });

    this._overlayRef = overlayRef;
  }

  public close(dialogResult?: R): void {
    this._result = dialogResult;

    this._overlayRef.dispose();
  }
}
